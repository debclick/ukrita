#package specific information for the desktop file
sed -i "/^Terminal=/i Icon=krita" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/krita is a digital canvas/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
